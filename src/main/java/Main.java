package Homework5;

public class Main {
    public static void main(String[] args) {
        String[] habits = {"run", "eat", "sleep"};
        String [][] schedule = {{"Sunday","Code"},{"Monday","Code"}};

        Homework5.Human mother = new Homework5.Human("Katerina", "Snowden", 1968,100,schedule);
        Homework5.Human father = new Homework5.Human("Edward", "Snowden", 1956,100,schedule);
        Homework5.Pet pet = new Homework5.Pet("dog","Alabash",3,100, habits);
        Homework5.Human child = new Homework5.Human("Steve","Snowden",1993,100,schedule);

        Homework5.Family family = new Homework5.Family(mother, father, 1);
        family.addChild(child);
        System.out.println(family.toString() + "\n");
        System.out.println(pet.toString() + "\n");
        System.out.println(child.toString());
    }
}